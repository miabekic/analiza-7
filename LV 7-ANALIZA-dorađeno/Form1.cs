﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV_7_ANALIZA
{
    public partial class Form1 : Form
    {
        String prvi_igrac, drugi_igrac;
        int rez1, rez2, nerijeseno, i;

        private void label_prvi_igrac_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label_drugi_igrac_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelrez1_Click(object sender, EventArgs e)
        {

        }

        private void labelrez2_Click(object sender, EventArgs e)
        {

        }

        private void labelodabir_polja_Click(object sender, EventArgs e)
        {

        }

        private void textBoxOdaberiPolje_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void labelRedIgraca_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void labelnerijeseno_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void labelcrtice_Click(object sender, EventArgs e)
        {

        }

        private void labelcrtice2_Click(object sender, EventArgs e)
        {

        }

        private void labelcrta_Click(object sender, EventArgs e)
        {

        }

        private void labelcrta2_Click(object sender, EventArgs e)
        {

        }

        private void buttonPOBJEDI_Click(object sender, EventArgs e)
        {
            if (textBoxOdaberiPolje.Text == "")
            {
                MessageBox.Show("NIJE UNESENO POLJE!", "POGRESKA!");
            }
             else  if (textBoxOdaberiPolje.Text == "1")
            {
                if (label1.Text == "X" || label1.Text == "O")
                {
                    MessageBox.Show("POLJE JE ZAUZETO, POKUSAJTE NEKO DRUGO!", "POKGRESKA!");
                    textBoxOdaberiPolje.Text = "";
                }
                else if (labelRedIgraca.Text == prvi_igrac && radioButtonX.Checked)
                {
                    label1.Text = "X";
                    labelRedIgraca.Text = drugi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
                 else if (labelRedIgraca.Text == drugi_igrac && radioButtonY.Checked)
                {
                    label1.Text = "O";
                    labelRedIgraca.Text = prvi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
            }
             else if (textBoxOdaberiPolje.Text == "2")
            {
                if (label2.Text == "X" || label2.Text == "O")
                {
                    MessageBox.Show("POLJE JE ZAUZETO, POKUSAJTE NEKO DRUGO!", "POKGRESKA!");
                    textBoxOdaberiPolje.Text = "";
                }
                else if (labelRedIgraca.Text == prvi_igrac && radioButtonX.Checked)
                {
                    label2.Text = "X";
                    labelRedIgraca.Text = drugi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
                else if (labelRedIgraca.Text == drugi_igrac && radioButtonY.Checked)
                {
                    label2.Text = "O";
                    labelRedIgraca.Text = prvi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
            }
            else if (textBoxOdaberiPolje.Text == "3")
            {
                if (label3.Text == "X" || label3.Text == "O")
                {
                    MessageBox.Show("POLJE JE ZAUZETO, POKUSAJTE NEKO DRUGO!", "POKGRESKA!");
                    textBoxOdaberiPolje.Text = "";
                }
                else if (labelRedIgraca.Text == prvi_igrac && radioButtonX.Checked)
                {
                    label3.Text = "X";
                    labelRedIgraca.Text = drugi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
                else if (labelRedIgraca.Text == drugi_igrac && radioButtonY.Checked)
                {
                    label3.Text = "O";
                    labelRedIgraca.Text = prvi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
            }
            else if (textBoxOdaberiPolje.Text == "4")
            {
                if (label4.Text == "X" || label4.Text == "O")
                {
                    MessageBox.Show("POLJE JE ZAUZETO, POKUSAJTE NEKO DRUGO!", "POKGRESKA!");
                    textBoxOdaberiPolje.Text = "";
                }
                else if (labelRedIgraca.Text == prvi_igrac && radioButtonX.Checked)
                {
                    label4.Text = "X";
                    labelRedIgraca.Text = drugi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
                else if (labelRedIgraca.Text == drugi_igrac && radioButtonY.Checked)
                {
                    label4.Text = "O";
                    labelRedIgraca.Text = prvi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
            }
           else if (textBoxOdaberiPolje.Text == "5")
            {
                if (label5.Text == "X" || label5.Text == "O")
                {
                    MessageBox.Show("POLJE JE ZAUZETO, POKUSAJTE NEKO DRUGO!", "POKGRESKA!");
                    textBoxOdaberiPolje.Text = "";
                }
               else if (labelRedIgraca.Text == prvi_igrac && radioButtonX.Checked)
                {
                    label5.Text = "X";
                    labelRedIgraca.Text = drugi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
                else if (labelRedIgraca.Text == drugi_igrac && radioButtonY.Checked)
                {
                    label5.Text = "O";
                    labelRedIgraca.Text = prvi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
            }
            else if (textBoxOdaberiPolje.Text == "6")
            {
                if (label6.Text == "X" || label6.Text == "O")
                {
                    MessageBox.Show("POLJE JE ZAUZETO, POKUSAJTE NEKO DRUGO!", "POKGRESKA!");
                    textBoxOdaberiPolje.Text = "";
                }
                else if (labelRedIgraca.Text == prvi_igrac && radioButtonX.Checked)
                {
                    label6.Text = "X";
                    labelRedIgraca.Text = drugi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
                else if (labelRedIgraca.Text == drugi_igrac && radioButtonY.Checked)
                {
                    label6.Text = "O";
                    labelRedIgraca.Text = prvi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
            }
           else if (textBoxOdaberiPolje.Text == "7")
            {
                if (label7.Text == "X" || label7.Text == "O")
                {
                    MessageBox.Show("POLJE JE ZAUZETO, POKUSAJTE NEKO DRUGO!", "POKGRESKA!");
                    textBoxOdaberiPolje.Text = "";
                }
                else if (labelRedIgraca.Text == prvi_igrac && radioButtonX.Checked)
                {
                    label7.Text = "X";
                    labelRedIgraca.Text = drugi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
                else if (labelRedIgraca.Text == drugi_igrac && radioButtonY.Checked)
                {
                    label7.Text = "O";
                    labelRedIgraca.Text = prvi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
            }
           else if (textBoxOdaberiPolje.Text == "8")
            {
                if (label8.Text == "X" || label8.Text == "O")
                {
                    MessageBox.Show("POLJE JE ZAUZETO, POKUSAJTE NEKO DRUGO!", "POKGRESKA!");
                    textBoxOdaberiPolje.Text = "";
                }
              else  if (labelRedIgraca.Text == prvi_igrac && radioButtonX.Checked)
                {
                    label8.Text = "X";
                    labelRedIgraca.Text = drugi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
                else if (labelRedIgraca.Text == drugi_igrac && radioButtonY.Checked)
                {
                    label8.Text = "O";
                    labelRedIgraca.Text = prvi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
            }
            else if (textBoxOdaberiPolje.Text == "9")
            {
                if (label9.Text == "X" || label9.Text == "O")
                {
                    MessageBox.Show("POLJE JE ZAUZETO, POKUSAJTE NEKO DRUGO!", "POKGRESKA!");
                    textBoxOdaberiPolje.Text = "";
                }
               else if (labelRedIgraca.Text == prvi_igrac && radioButtonX.Checked)
                {
                    label9.Text = "X";
                    labelRedIgraca.Text = drugi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
                else if (labelRedIgraca.Text == drugi_igrac && radioButtonY.Checked)
                {
                    label9.Text = "O";
                    labelRedIgraca.Text = prvi_igrac;
                    textBoxOdaberiPolje.Text = "";
                    i++;
                }
            }
            if (label1.Text == "X" && label2.Text == "X" && label3.Text == "X" || label1.Text == "X" && label4.Text == "X" && label7.Text == "X" || label1.Text == "X" && label5.Text == "X" && label9.Text == "X" || label2.Text == "X" && label5.Text == "X" && label8.Text == "X" || label3.Text == "X" && label6.Text == "X" && label9.Text == "X" || label4.Text == "X" && label5.Text == "X" && label6.Text == "X" || label7.Text == "X" && label8.Text == "X" && label9.Text == "X" || label3.Text == "X" && label5.Text == "X" && label7.Text == "X")
            {
                MessageBox.Show("Pobijedili ste! Cestitamo!", "KRAJ");
                rez1 += 1;
                labelrez1.Text = rez1.ToString();
            }
            if (label1.Text == "O" && label2.Text == "O" && label3.Text == "O" || label1.Text == "O" && label4.Text == "O" && label7.Text == "O" || label1.Text == "O" && label5.Text == "O" && label9.Text == "O" || label2.Text == "O" && label5.Text == "O" && label8.Text == "O" || label3.Text == "O" && label6.Text == "O" && label9.Text == "O" || label4.Text == "O" && label5.Text == "O" && label6.Text == "O" || label7.Text == "O" && label8.Text == "O" && label9.Text == "O" || label3.Text == "O" && label5.Text == "O" && label7.Text == "O")
            {
                MessageBox.Show("Pobijedili ste! Cestitamo!", "KRAJ");
                rez2 += 1;
                labelrez2.Text = rez2.ToString();
            }
            if (i == 9)
            {
                MessageBox.Show("NERIJESENO!", "KRAJ");
                nerijeseno += 1;
                labelnerijeseno.Text = nerijeseno.ToString();
            }
        }

        private void buttonQuit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonAgain_Click(object sender, EventArgs e)
        {
            i = 0;
            label1.Text = "";
            label2.Text = "";
            label3.Text = "";
            label4.Text = "";
            label5.Text = "";
            label6.Text = "";
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if(textBox1.Text=="" || textBox2.Text == "")
            {
                MessageBox.Show("NIJE UNESEN JEDAN OD IGRACA!", "POGRESKA");
            }
            i = 0;
            rez1 = 0;
            rez2 = 0;
            nerijeseno = 0;
            label1.Text = "";
            label2.Text = "";
            label3.Text = "";
            label4.Text = "";
            label5.Text = "";
            label6.Text = "";
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
            labelrez1.Text = rez1.ToString();
            labelrez2.Text = rez2.ToString();
            labelnerijeseno.Text = nerijeseno.ToString();
            prvi_igrac = textBox1.Text;
            drugi_igrac = textBox2.Text;
            labelRedIgraca.Text = prvi_igrac;
            
           
        }

    }
}
