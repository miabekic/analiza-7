﻿namespace LV_7_ANALIZA
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_prvi_igrac = new System.Windows.Forms.Label();
            this.label_drugi_igrac = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelcrta2 = new System.Windows.Forms.Label();
            this.labelcrta = new System.Windows.Forms.Label();
            this.labelcrtice2 = new System.Windows.Forms.Label();
            this.labelcrtice = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelRedIgraca = new System.Windows.Forms.Label();
            this.labelodabir_polja = new System.Windows.Forms.Label();
            this.textBoxOdaberiPolje = new System.Windows.Forms.TextBox();
            this.labelrez1 = new System.Windows.Forms.Label();
            this.labelrez2 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelnerijeseno = new System.Windows.Forms.Label();
            this.buttonAgain = new System.Windows.Forms.Button();
            this.buttonQuit = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonPOBJEDI = new System.Windows.Forms.Button();
            this.radioButtonX = new System.Windows.Forms.RadioButton();
            this.radioButtonY = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_prvi_igrac
            // 
            this.label_prvi_igrac.AutoSize = true;
            this.label_prvi_igrac.Location = new System.Drawing.Point(12, 19);
            this.label_prvi_igrac.Name = "label_prvi_igrac";
            this.label_prvi_igrac.Size = new System.Drawing.Size(89, 17);
            this.label_prvi_igrac.TabIndex = 0;
            this.label_prvi_igrac.Text = "1. IGRAČ (X)";
            this.label_prvi_igrac.Click += new System.EventHandler(this.label_prvi_igrac_Click);
            // 
            // label_drugi_igrac
            // 
            this.label_drugi_igrac.AutoSize = true;
            this.label_drugi_igrac.Location = new System.Drawing.Point(12, 62);
            this.label_drugi_igrac.Name = "label_drugi_igrac";
            this.label_drugi_igrac.Size = new System.Drawing.Size(91, 17);
            this.label_drugi_igrac.TabIndex = 1;
            this.label_drugi_igrac.Text = "2. IGRAČ (O)";
            this.label_drugi_igrac.Click += new System.EventHandler(this.label_drugi_igrac_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(105, 16);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(105, 59);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 3;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.labelcrta2);
            this.panel1.Controls.Add(this.labelcrta);
            this.panel1.Controls.Add(this.labelcrtice2);
            this.panel1.Controls.Add(this.labelcrtice);
            this.panel1.Location = new System.Drawing.Point(302, 130);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(389, 274);
            this.panel1.TabIndex = 4;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(296, 223);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 17);
            this.label9.TabIndex = 15;
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(166, 223);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 17);
            this.label8.TabIndex = 14;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(39, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 17);
            this.label7.TabIndex = 13;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(296, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 17);
            this.label6.TabIndex = 12;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(166, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 17);
            this.label5.TabIndex = 11;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 17);
            this.label4.TabIndex = 10;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(296, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 17);
            this.label3.TabIndex = 9;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 8;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 7;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelcrta2
            // 
            this.labelcrta2.AutoSize = true;
            this.labelcrta2.Location = new System.Drawing.Point(2, 172);
            this.labelcrta2.Name = "labelcrta2";
            this.labelcrta2.Size = new System.Drawing.Size(384, 17);
            this.labelcrta2.TabIndex = 3;
            this.labelcrta2.Text = "_______________________________________________";
            this.labelcrta2.Click += new System.EventHandler(this.labelcrta2_Click);
            // 
            // labelcrta
            // 
            this.labelcrta.AutoSize = true;
            this.labelcrta.Location = new System.Drawing.Point(2, 78);
            this.labelcrta.Name = "labelcrta";
            this.labelcrta.Size = new System.Drawing.Size(384, 17);
            this.labelcrta.TabIndex = 2;
            this.labelcrta.Text = "_______________________________________________";
            this.labelcrta.Click += new System.EventHandler(this.labelcrta_Click);
            // 
            // labelcrtice2
            // 
            this.labelcrtice2.AutoSize = true;
            this.labelcrtice2.Location = new System.Drawing.Point(254, 9);
            this.labelcrtice2.Name = "labelcrtice2";
            this.labelcrtice2.Size = new System.Drawing.Size(11, 255);
            this.labelcrtice2.TabIndex = 0;
            this.labelcrtice2.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n";
            this.labelcrtice2.Click += new System.EventHandler(this.labelcrtice2_Click);
            // 
            // labelcrtice
            // 
            this.labelcrtice.AutoSize = true;
            this.labelcrtice.Location = new System.Drawing.Point(122, 9);
            this.labelcrtice.Name = "labelcrtice";
            this.labelcrtice.Size = new System.Drawing.Size(11, 255);
            this.labelcrtice.TabIndex = 1;
            this.labelcrtice.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r";
            this.labelcrtice.Click += new System.EventHandler(this.labelcrtice_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(24, 199);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(136, 17);
            this.label14.TabIndex = 7;
            this.label14.Text = "Igrač koji je na redu:";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // labelRedIgraca
            // 
            this.labelRedIgraca.AutoSize = true;
            this.labelRedIgraca.Location = new System.Drawing.Point(24, 244);
            this.labelRedIgraca.Name = "labelRedIgraca";
            this.labelRedIgraca.Size = new System.Drawing.Size(0, 17);
            this.labelRedIgraca.TabIndex = 8;
            this.labelRedIgraca.Click += new System.EventHandler(this.labelRedIgraca_Click);
            // 
            // labelodabir_polja
            // 
            this.labelodabir_polja.AutoSize = true;
            this.labelodabir_polja.Location = new System.Drawing.Point(378, 22);
            this.labelodabir_polja.Name = "labelodabir_polja";
            this.labelodabir_polja.Size = new System.Drawing.Size(128, 17);
            this.labelodabir_polja.TabIndex = 9;
            this.labelodabir_polja.Text = "Odaberi polje (1-9)";
            this.labelodabir_polja.Click += new System.EventHandler(this.labelodabir_polja_Click);
            // 
            // textBoxOdaberiPolje
            // 
            this.textBoxOdaberiPolje.Location = new System.Drawing.Point(544, 19);
            this.textBoxOdaberiPolje.Name = "textBoxOdaberiPolje";
            this.textBoxOdaberiPolje.Size = new System.Drawing.Size(100, 22);
            this.textBoxOdaberiPolje.TabIndex = 10;
            this.textBoxOdaberiPolje.TextChanged += new System.EventHandler(this.textBoxOdaberiPolje_TextChanged);
            // 
            // labelrez1
            // 
            this.labelrez1.AutoSize = true;
            this.labelrez1.Location = new System.Drawing.Point(260, 19);
            this.labelrez1.Name = "labelrez1";
            this.labelrez1.Size = new System.Drawing.Size(0, 17);
            this.labelrez1.TabIndex = 11;
            this.labelrez1.Click += new System.EventHandler(this.labelrez1_Click);
            // 
            // labelrez2
            // 
            this.labelrez2.AutoSize = true;
            this.labelrez2.Location = new System.Drawing.Point(260, 64);
            this.labelrez2.Name = "labelrez2";
            this.labelrez2.Size = new System.Drawing.Size(0, 17);
            this.labelrez2.TabIndex = 12;
            this.labelrez2.Click += new System.EventHandler(this.labelrez2_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(24, 302);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 17);
            this.label19.TabIndex = 13;
            this.label19.Text = "Neriješeno:";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // labelnerijeseno
            // 
            this.labelnerijeseno.AutoSize = true;
            this.labelnerijeseno.Location = new System.Drawing.Point(115, 302);
            this.labelnerijeseno.Name = "labelnerijeseno";
            this.labelnerijeseno.Size = new System.Drawing.Size(0, 17);
            this.labelnerijeseno.TabIndex = 14;
            this.labelnerijeseno.Click += new System.EventHandler(this.labelnerijeseno_Click);
            // 
            // buttonAgain
            // 
            this.buttonAgain.Location = new System.Drawing.Point(27, 393);
            this.buttonAgain.Name = "buttonAgain";
            this.buttonAgain.Size = new System.Drawing.Size(133, 23);
            this.buttonAgain.TabIndex = 15;
            this.buttonAgain.Text = "IGRAJ PONOVO";
            this.buttonAgain.UseVisualStyleBackColor = true;
            this.buttonAgain.Click += new System.EventHandler(this.buttonAgain_Click);
            // 
            // buttonQuit
            // 
            this.buttonQuit.Location = new System.Drawing.Point(713, 404);
            this.buttonQuit.Name = "buttonQuit";
            this.buttonQuit.Size = new System.Drawing.Size(75, 23);
            this.buttonQuit.TabIndex = 16;
            this.buttonQuit.Text = "IZLAZ";
            this.buttonQuit.UseVisualStyleBackColor = true;
            this.buttonQuit.Click += new System.EventHandler(this.buttonQuit_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(699, 56);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(89, 63);
            this.buttonStart.TabIndex = 17;
            this.buttonStart.Text = "POKRENI IGRU!";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonPOBJEDI
            // 
            this.buttonPOBJEDI.Location = new System.Drawing.Point(544, 64);
            this.buttonPOBJEDI.Name = "buttonPOBJEDI";
            this.buttonPOBJEDI.Size = new System.Drawing.Size(90, 23);
            this.buttonPOBJEDI.TabIndex = 18;
            this.buttonPOBJEDI.Text = "POKUŠAJ";
            this.buttonPOBJEDI.UseVisualStyleBackColor = true;
            this.buttonPOBJEDI.Click += new System.EventHandler(this.buttonPOBJEDI_Click);
            // 
            // radioButtonX
            // 
            this.radioButtonX.AutoSize = true;
            this.radioButtonX.Location = new System.Drawing.Point(15, 122);
            this.radioButtonX.Name = "radioButtonX";
            this.radioButtonX.Size = new System.Drawing.Size(38, 21);
            this.radioButtonX.TabIndex = 19;
            this.radioButtonX.TabStop = true;
            this.radioButtonX.Text = "X";
            this.radioButtonX.UseVisualStyleBackColor = true;
            // 
            // radioButtonY
            // 
            this.radioButtonY.AutoSize = true;
            this.radioButtonY.Location = new System.Drawing.Point(15, 149);
            this.radioButtonY.Name = "radioButtonY";
            this.radioButtonY.Size = new System.Drawing.Size(38, 21);
            this.radioButtonY.TabIndex = 20;
            this.radioButtonY.TabStop = true;
            this.radioButtonY.Text = "Y";
            this.radioButtonY.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.radioButtonY);
            this.Controls.Add(this.radioButtonX);
            this.Controls.Add(this.buttonPOBJEDI);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonQuit);
            this.Controls.Add(this.buttonAgain);
            this.Controls.Add(this.labelnerijeseno);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.labelrez2);
            this.Controls.Add(this.labelrez1);
            this.Controls.Add(this.textBoxOdaberiPolje);
            this.Controls.Add(this.labelodabir_polja);
            this.Controls.Add(this.labelRedIgraca);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label_drugi_igrac);
            this.Controls.Add(this.label_prvi_igrac);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_prvi_igrac;
        private System.Windows.Forms.Label label_drugi_igrac;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelcrta2;
        private System.Windows.Forms.Label labelcrta;
        private System.Windows.Forms.Label labelcrtice2;
        private System.Windows.Forms.Label labelcrtice;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelRedIgraca;
        private System.Windows.Forms.Label labelodabir_polja;
        private System.Windows.Forms.TextBox textBoxOdaberiPolje;
        private System.Windows.Forms.Label labelrez1;
        private System.Windows.Forms.Label labelrez2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labelnerijeseno;
        private System.Windows.Forms.Button buttonAgain;
        private System.Windows.Forms.Button buttonQuit;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonPOBJEDI;
        private System.Windows.Forms.RadioButton radioButtonX;
        private System.Windows.Forms.RadioButton radioButtonY;
    }
}

